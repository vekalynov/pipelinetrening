# в качестве базового образа используем java:8,
#добавляем наш Java-класс (команда ADD),
# компилируем его (при помощи команды RUN)
# и указываем команду, которая выполнится при запуске контейнера (команда CMD)
FROM openjdk:8-jdk-alpine
ADD src/main/java/HelloWorld.java .
RUN javac HelloWorld.java
CMD ["java", "HelloWorld"]