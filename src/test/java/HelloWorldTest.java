import org.junit.Assert;
import org.junit.Test;


public class HelloWorldTest {

    private HelloWorld helloWorld = new HelloWorld();

    @Test
    public void helloWorld() {
        String expected = "Hello world";
        String actual = helloWorld.getHelloWorld();
        Assert.assertEquals(actual,expected);
    }
}